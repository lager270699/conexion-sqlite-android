package com.senati.appseminario.util;

public class Constantes {
    public static final String DB_NAME="db_asistentes.db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME ="ASISTENTES";
    public static final String COLUMN_ID="ID_ASIS";
    public static final String COLUMN_NOMBRES="NOMBRES_ASIS";
    public static final String COLUMN_APELLIDOS="APELLIDOS_ASIS";
    public static final String COLUMN_FECNAC="FECNAC_ASIS";
    public static final String COLUMN_DNI="DNI_ASIS";
    public static final String COLUMN_SEXO="SEXO_ASIS";
    public static final String COLUMN_EMAIL="EMAIL_ASIS";
    public static final String COLUMN_TELEFONO="TELEFONO_ASIS";

    public static final String SCRIPT_CREATE ="CREATE TABLE "+TABLE_NAME+" ( "
            +COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
            +COLUMN_NOMBRES+ " TEXT, "
            +COLUMN_APELLIDOS+ " TEXT, "
            +COLUMN_FECNAC+ " TEXT, "
            +COLUMN_DNI+" TEXT, "
            +COLUMN_SEXO+" TEXT, "
            +COLUMN_EMAIL+" TEXT, "
            +COLUMN_TELEFONO+" TEXT )";

    public static final String SCRIPT_DROP ="DROP TABLE IS EXISTS "+TABLE_NAME;

}
